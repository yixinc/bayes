%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Gibbs sampling example
%    Implements a Gibbs
%     sampler for the 
%     mean and variance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function [mu,phi] = Gibbs_Normal(x, trueMu, trueSigma)
 
%set basical functionality controls
trace=1;
 
thin = 1;
step = 1; %proposal variance
iterations = 1000000;
burnin = 500000;
 
muInit= -100;%trueMu;
phiInit=5;%1/trueSigma^2;
 
%Note: priors...
%priorMu = 1;
%priorVar = 1/sigma^2; 
 
 
%%Create space for the stored variables
mu = zeros(1,iterations);
phi = zeros(1,iterations);
 
%initialize
mu(1) = muInit;
phi(1) = phiInit;
 
%basic plotting of the "truth"... true inputs are only used here
figure(1)
clf
plot(trueMu, 1/trueSigma^2,'r.')
xlabel('\mu','fontsize',14)
ylabel('\phi','fontsize',14)
pause
hold on
 
 
%Gibbs Sample (This really is using an embedded M-H sampler for
%instructional purposes... More on that LATER:)).
tic
for(i=2:iterations)
 
    %%update the mean (Sample from Normal(\bar x),1/(N\phi)), but in an MH
    %%step
    proposalMu = mu(i-1) + randn*step/10;
 
    %log scale??    
    %proposed = log(prod((1/sqrt(2*pi)*sqrt(phi(i-1))*exp(-phi(i-1)/2*(x-proposalMu).^2))));
    %currentlyAt = log(prod((1/sqrt(2*pi)*sqrt(phi(i-1))*exp(-phi(i-1)/2*(x-mu(i-1)).^2))));
    proposed    = sum( - phi(i-1)*(x-proposalMu).^2/2);
    currentlyAt = sum( - phi(i-1)*(x-mu(i-1)).^2/2); 
    
    ratio = proposed-currentlyAt;
    if(log(rand)<ratio)
        mu(i) = proposalMu;
    else
        mu(i) = mu(i-1);
    end
 
    %%update the Precision (Sample from Gamma(N/2, 0.5*\sum(x-\mu))), but
    %%in an MH step
    proposalPhi=-1;
    while(proposalPhi<0) % note... this is not a symmetric proposal!
        proposalPhi = phi(i-1) + randn*step*2;
    end
 
    proposed    = -log(proposalPhi)+sum(log( sqrt(proposalPhi)/sqrt(2*pi))  - proposalPhi*(x-mu(i)).^2/2);
    currentlyAt = -log(phi(i-1))+sum(log(sqrt(phi(i-1))/sqrt(2*pi))  - phi(i-1)*(x-mu(i)).^2/2);  
     
    
    
    ratio = proposed-currentlyAt; %This Ratio Needs a correction: Send me the correction
    if(log(rand)<ratio)
        phi(i) = proposalPhi;
    else
        phi(i) = phi(i-1);
    end
 
 
   plotStep=10000; 
   if(mod(i,plotStep)==0)
        figure(1)
        clf
        xlabel('\mu','fontsize',18)
        ylabel('\phi','fontsize',18)
        i
        hold on
        plot(mu(1:i),phi(1:i),'-','color',[100/255,100/255,100/255])
        plot(mu(i-plotStep+1:i),phi(i-plotStep+1:i),'k-')
        plot(trueMu, 1/trueSigma^2,'r*')
        pause(.1)
   end
 
end
toc
 
 
if(trace==1)
    figure(2)
    clf;
    subplot(2,1,1)
    mu = mu(1:thin:length(mu));         %% thin samples
    plot(mu)
    xlabel('Iterations','fontsize',18)
    ylabel('\mu','fontsize',18)
    mu = mu((burnin+1):length(mu));     %% chop off burnin 
 
    subplot(2,1,2)
    %normalizedHist(mu,.2)
    hist(mu)
    hold on;
    xlabel('\mu')
    plot([trueMu,trueMu],[0,length(mu)/10],'r','linewidth',2)
 
    figure(3)
    clf;
    subplot(2,1,1)
    phi = phi(1:thin:length(phi));      %% thin samples
    plot(phi)
    xlabel('Iterations','fontsize',18)
    ylabel('\phi','fontsize',18)
    
    length(phi)
    phi = phi((burnin+1):length(phi));  %% chop off burnin
    phi;
    
    subplot(2,1,2)
    %normalizedHist(phi,.1)
    hist(phi)
    xlabel('\phi')
    hold on;
    plot([1/trueSigma^2,1/trueSigma^2],[0,length(mu)/10],'r','linewidth',2)
    1/trueSigma^2
    
    figure(4)
    plot(mu,phi,'ko')
    xlabel('\mu','fontsize',18)
    ylabel('\phi','fontsize',18)
    
end    
