# STAT5444 Bayesian Inference

+ Yixin Chen(<yixinc@vt.edu>)

## Details

This repository contains homework solutions and class notes of STAT5444 Bayesian Inference in Fall 2021 at Virginia Tech.