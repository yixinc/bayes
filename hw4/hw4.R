# Problem 1 Gibbs Sampler for Rat Data
#
## Import data
library(readr)
library(mvtnorm)
dat <- read_table2("RatData.txt", col_names = FALSE)[,-c(1,7)]
colnames(dat) <- c("time1","time2","time3","time4","time5")
dat <- as.matrix(dat)

set.seed(69824315)
# exploratory
x <- seq(1,5)
para <- matrix(NA, nrow=nrow(dat), ncol=2)
for (i in 1:nrow(dat)) {
  y <- dat[i,]
  para[i,] <- lm(y~x)$coefficients
}
init.mean <- apply(para, 2, mean)
#init.var <- apply(para, 2, var)

## Initialize
n <- nrow(dat) # i = 1, 2, ..., n
t <- ncol(dat) # j = 1, 2, ..., t
a <- 0
b <- 0
eta <- init.mean                     # mean vector of theta0 prior
#eta <- c(100,40)  
invPsi <- matrix(rep(0,4), nrow = 2) # covar matrix of theta0 prior
rho <- 2                             # prior para of invSig 
R <- matrix(c(100,0,0,0.1),nrow=2)   # prior para of invSig
theta0.0 <- init.mean
phi.0 <- 0.1
invSig.0 <- diag(2)

## Gibbs sampling
# Pre-allocate
maxit <- 5000
theta0 <- matrix(NA, nrow = maxit, ncol = 2)
theta0[1,] <- theta0.0
#theta0[1,] <- c(100,40)
phi <- rep(NA, maxit)
phi[1] <- phi.0
invSig <- array(NA, c(2,2,maxit))
invSig[,,1] <- invSig.0

# Prep for neat coding
tm <- cbind(rep(1,5), x)
stt <- crossprod(tm, tm) # sum_j t_j %*% t_j^T
sxt <- dat %*% tm        # sum_j x_ij * t_j

# Begin
for (m in 2:maxit)
{
  #update theta_i
  thetai <- matrix(NA, nrow = n, ncol = 2) # reset at the beginning of every iteration
  for (i in 1:n) 
  { 
    V <- solve(invSig[,,m-1] + phi[m-1]*stt)
    mu <- V %*% (invSig[,,m-1] %*% theta0[m-1,] + phi[m-1]*sxt[i,] )
    thetai[i,] <- rmvnorm(1, mu, V)
  }
  
  #update phi
  phi[m] <- rgamma(1, 0.5*n*t+a, 0.5*sum((dat - thetai %*% t(tm))^2)+b )
  
  # update theta_0
  V <- solve(n*invSig[,,m-1] + invPsi)
  mu <- V %*% (invSig[,,m-1] %*% colSums(thetai) + invPsi %*% eta)
  theta0[m,] <- rmvnorm(1, mu, V)
  
  # update invSig
  diff <- t(thetai) - theta0[m,]
  V <- solve(tcrossprod(diff,diff) + rho*R)
  invSig[,,m] <- rWishart(1, rho+n, V)
}

eig<-function(mat){return(eigen(mat)$values)}
eigv<-apply(invSig, 3, eig)

pdf("beta_trace.pdf")
par(mfrow = c(2,1), mar=numeric(4),oma = c(3, 4, .5, .5), cex.axis=1.3)
plot(theta0[,1],type = "l", lwd = 0.1, axes = F)
axis(2L)
box()
plot(theta0[,2],type = "l", lwd = 0.1, axes = F)
axis(1L)
axis(2L)
box()
mtext("iteration",side = 1, outer = T,line = 2, cex=1.5)
mtext(bquote(alpha[0]),side = 2, outer = T,line = 2, adj = 1/4, cex=1.5)
mtext(bquote(beta[0]),side = 2, outer = T,line = 2, adj = 3/4, cex=1.5)
dev.off()

pdf("eig_trace.pdf")
par(mfrow = c(2,1), mar=numeric(4),oma = c(3, 4, .5, .5), cex.axis=1.3)
plot(eigv[1,],type = "l", lwd = 0.1, axes = F)
axis(2L)
box()
plot(eigv[2,], ylim = c(0,0.05), type = "l", lwd = 0.1, axes = F)
axis(1L)
axis(2L)
box()
mtext("iteration",side = 1, outer = T,line = 2, cex=1.5)
mtext("First Eigenvalue", side = 2, outer = T,line = 2.2, adj = 6/7, cex = 1.5)
mtext("Second Eigenvalue", side = 2, outer = T,line = 2.2, adj = 1/7, cex = 1.5)
dev.off()

pdf("phi_trace.pdf",width=14,height = 3.5)
par(mfrow = c(1,1), mar=numeric(4),oma = c(3, 4, .5, .5), cex.axis=1.3)
plot(phi,type = "l", ylim =  c(0.005,0.017), axes = F, lwd = 0.1)
axis(1L);axis(2L);box()
mtext("iteration",side = 1, outer = T,line = 2, cex=1.5)
mtext(bquote(phi), side = 2, outer = T,line = 2, cex=1.5)
dev.off()

pdf("beta_dens.pdf", width = 8, height = 4)
par(mfrow = c(1,2), mar=numeric(4),oma = c(3, 4, .5, .5), cex.axis=0.8)
plot(density(theta0[,1]),main = "", axes = F, ylim=c(0,0.6), lwd=1.2)
axis(2L)
axis(1L)
box()
plot(density(theta0[,2]),main = "", axes = F, ylim=c(0,0.6), lwd=1.2)
axis(1L)
box()
mtext("density",side = 2, outer = T,line = 2)
mtext(bquote(alpha[0]),side = 1, outer = T,line = 2, adj = 1/4)
mtext(bquote(beta[0]),side = 1, outer = T,line = 2, adj = 3/4)
dev.off()

save(theta0, phi, eigv, file="p1.RData")
save.image()

# Problem 2 Gibbs Sampler for Cauchy Regression
## Generate data
library(emulator)
n <- 1000
Sig <- matrix(c(1,0.8,0.8,1),2,2)
dat <- rmvt(n, Sig, 1)
x<-dat[,1]
X <- cbind(rep(1,n), dat[,1])
y <- dat[,2]

# Initialize
maxit <- 5000
beta <- matrix(NA,nrow = maxit, ncol = 2)
beta[1, ] <- c(0, 1)
phi <- rep(NA, maxit)
phi[1] <- 1

# Gibbs sampler
for (m in 2:maxit) {
  # update gamma_i's
  rate <- 0.5*phi[m-1]*(y - X%*%beta[m-1,])^2 + 0.5
  gammai <- rgamma(n, 1, rate)
  
  # update beta vector
  W <- diag(gammai)
  V <- solve(phi[m-1]*quad.form(W,X))
  mu <- solve(quad.form(W,X)) %*% crossprod(X,W) %*%y
  beta[m,] <- rmvnorm(1, mu, V)
  
  # update \phi
  rate <- 0.5*quad.form(W, y-X%*%beta[m,])
  phi[m] <- rgamma(1, 0.5*n, rate)
}
burnin <- 50
beta <- beta[-(1:burnin),]

pdf("beta_hist.pdf", width = 12, height = 6)
par(mfrow = c(1,2), mar=numeric(4),oma = c(4, 4.5, 1, 1)+0.1, cex.axis=1.5)
hist(beta[,1], main = "", axes = F, ylim = c(0,1500))
axis(2L,lwd = 0.5);axis(1L,lwd = 0.5);box()
hist(beta[,2], main = "", axes = F, ylim = c(0,1500))
axis(1L,lwd = 0.5);box()
mtext(bquote(beta[0]),side = 1, outer = T,line = 3, adj = 1/4, cex=1.5)
mtext(bquote(beta[1]),side = 1, outer = T,line = 3, adj = 3/4, cex=1.5)
mtext("Frequency",side = 2, outer = T,line = 3, cex=1.5)
dev.off()


# Prob 3 Hypothesis Testing for Gaussian Mixture
## Simulate data
n <- c(400,200,300,100)
theta <- c(10,15,17,20)
sig<-5
dat<-c()
for (i in 1:length(n)) {
  dat <- c(dat, rnorm(n[i], theta[i], sqrt(sig)))
}
# plot histogram
pdf("plots/mix_hist.pdf")
par(mfrow = c(1,1), mar=numeric(4),oma = c(4, 4.5, 1, 1)+0.1, cex.axis=1.5)
hist(dat, main = "", axes = F, ylim = c(0,100), breaks = 20)
axis(2L,lwd = 0.5);axis(1L,lwd = 0.5);box(lwd=1)
mtext("x",side = 1, outer = T,line = 3, cex=1.5)
mtext("Frequency",side = 2, outer = T,line = 3, cex=1.5)
dev.off()

## Randomize the order of simulated data
rdat <- sample(dat, size = length(dat), replace = F)
#plot the data against 1:1000
pdf("plots/rand_dat.pdf")
par(mar=c(4,4.5,1,1)+0.1)
plot(1:1000,rdat,pch=16,col=rgb(0,0,0,0.5), xlab="i",ylab="randomized x",
     cex.lab = 1.5, cex.axis=1.5, cex = 1.5)
dev.off()

## Calculate Posterior Probabilities Sequentially
#load("RData/p3.RData")
library(matrixStats)
#preset <- array(rbind(n,theta), dim = c(2,1,4))
mysum<-function(theta, data) { sum( (data - theta)^2 )/(2*sig)  }
seq.pos <- function(theta0, data, thetas)
{ 
  o <- which(theta == theta0)
  L <- length(data)
  pos<-rep(NA, L)
  for (i in 1:L) {
    negsums <- -sapply(theta, mysum, data[1:i])
    logbf <- log(3) + negsums[o] - logSumExp(negsums[-o])
    pos[i] <- 1/( 1 + 3/exp(logbf) ) 
  }
  return(pos)
}
postprob <- sapply(theta, seq.pos, data = rdat, thetas=theta) 

# # Plot the Posterior Probability
# pdf("plots/postprob0.pdf", width = 16, height = 5)
# par(mfrow = c(1,4), mar=numeric(4),oma = c(4, 5, 5, 1), cex.axis=1.5)
# for (i in 1:4) {
#   plot(postprob[,i], type= "l", axes = F, ylim=c(0,1), lwd=1.2)
#   axis(1L)
#   if(i==1){axis(2L)}
#   box()
#   mtext(bquote(H[0]~":"~theta~"="~.(theta[i])), side = 3, outer = T,line = 2, adj = 1/12+(i-1)/3.6,cex=1.5)
# }
# mtext("Posterior Probability",side = 2, outer = T,line = 3, cex=1.5)
# mtext("number of data points",side = 1, outer = T,line = 3, cex=1.5)
# dev.off()

pdf("plots/postprob.pdf", width = 16, height = 5)
par(mfrow = c(1,4), mar=numeric(4),oma = c(4.2, 5, 5, 1)+0.1, cex.axis=1.5)
for (i in 1:4) {
  plot(postprob[,i], type= "l", axes = F, ylim=c(0,1), lwd=1.2)
  axis(1L)
  if(i==1){axis(2L)}
  box()
  mtext(bquote(H[0]~":"~theta~"="~.(theta[i])), side = 3, outer = T, line = 3,
        adj = c(0.095,0.37,0.635,0.9)[i], cex = 1.2)
  mtext(bquote(H[a]~":"~theta~"="~.(paste(theta[-i],collapse=","))), side = 3, 
        outer = T, line = 1, adj = c(1/12,0.37,0.64,0.91)[i], cex = 1.2)
}
mtext("Posterior Probability",side = 2, outer = T,line = 3, cex=1.5)
mtext("number of data points",side = 1, outer = T,line = 3, cex=1.5)
dev.off()

# Test using p-value
seq.pval <- function(theta, data)
{
  L <- length(data)
  pval<-rep(NA,L)
  for (i in 1:L) {
    z <- (mean(data[1:i]) - theta) / sqrt(sig/i)
    pval[i] <- 2*pnorm(abs(z),lower.tail = F)
  }
  return(pval)
}
pval <- sapply(theta, seq.pval, rdat)

pdf("plots/pval.pdf", width = 16, height = 5)
par(mfrow = c(1,4), mar=numeric(4),oma = c(4.2, 5, 5, 1)+0.1, cex.axis=1.5)
for (i in 1:4) {
  plot(pval[,i], type= "l", axes = F, ylim=c(0,1), lwd=1.2)
  axis(1L)
  if(i==1){axis(2L)}
  box()
  mtext(bquote(H[0]~":"~theta~"="~.(theta[i])), side = 3, outer = T, line = 3,
        adj = c(0.095,0.37,0.635,0.9)[i], cex = 1.2)
  mtext(bquote(H[a]~":"~theta~"="~.(paste(theta[-i],collapse=","))), side = 3, 
        outer = T, line = 1, adj = c(1/12,0.37,0.64,0.91)[i], cex = 1.2)
}
mtext("p-value",side = 2, outer = T,line = 3, cex=1.5)
mtext("number of data points",side = 1, outer = T,line = 3, cex=1.5)
dev.off()
